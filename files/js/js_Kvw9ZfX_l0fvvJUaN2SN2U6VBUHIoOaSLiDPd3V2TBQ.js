jQuery.fn.preload = function() {
    this.each(function(){
        jQuery('<img/>')[0].src = this;
    });
}
var globWindowHeight = 0;
var glodWrapperWidth = 0;
    
function ResizeIframe() {
    var $ = jQuery;
    var ratio = 1.641;
    var windowHeight = $(window).height();
    var windowWidth = $(window).width();
    if ($('#block-views-video-block .owl-item').length > 0)
        var maxWrapperWidth = $('#block-views-video-block .owl-item').width() - 20;
    else
        var maxWrapperWidth = $('#block-views-video-block .view-id-video').width() - 20;
//    console.log(globWindowHeight);
//    if (globWindowHeight > 0 && globWindowWidth > 0) {
//        if (globWindowHeight/windowHeight > 0.8 && globWindowHeight/windowHeight < 1.2 || 
//            glodWrapperWidth/maxWrapperWidth > 0.95 && glodWrapperWidth/maxWrapperWidth < 1.05)
//            return;
//    }
//    globWindowHeight = windowHeight;
//    glodWrapperWidth = maxWrapperWidth;
//    console.log("maxWrapperWidth: "+maxWrapperWidth);
    var maxWrapperHeight = maxWrapperWidth / ratio;
    if (windowHeight/maxWrapperHeight < 0.8) {
        maxWrapperHeight = windowHeight * 0.8;
        maxWrapperWidth = maxWrapperHeight * ratio;
    }
    $('#block-views-video-block iframe').each(function() {
        $(this).width(maxWrapperWidth);
        $(this).height(maxWrapperHeight);
    });
    var musicHeight = 580;
    if (musicHeight > (windowHeight * 0.9)) {
        musicHeight = windowHeight * 0.9;
    }
    $('#block-block-4 iframe, .iframe-wrapper iframe').each(function() {
        $(this).height(musicHeight);
    });
    $('#block-views-artists-block .views-field-field-image img').css('height','');
    $('#block-views-artists-block .owl-item .views-row').attr('style','');
    if (windowHeight > windowWidth) {
        var titleHeight = $('#block-views-artists-block .views-field-title').height();
        var imgHeight = (windowHeight - (titleHeight * 2) - 80 ) / 2;
        if ($('#block-views-artists-block .views-field-field-image img').height() > imgHeight) {
            $('#block-views-artists-block .views-field-field-image img').height(imgHeight);
        }
    } else if (windowWidth <= 680) {
        $('#block-views-artists-block .owl-item .views-row').attr('style','float: left; margin: 0 2%; width: 46%;');
    }
}

jQuery(document).ready(function ($) {
    
    $(['/sites/ultramusic.com/themes/ultramusic/css/images/ultra-radio_hover.png','/sites/ultramusic.com/themes/ultramusic/css/icons/social-icons.png']).preload();
    
    $('#block-system-main-menu .menu a').each(function() {
        $(this).click(function() {
            var rel = $(this).attr('rel');
            $('html, body').animate({
                scrollTop: $("#"+rel).offset().top
            }, 2000);
        });
    });
    if ($('#block-views-artists-block .view-content').length !== 0) {
        $('#block-views-artists-block .view-content').owlCarousel({
            itemsCustom : [
              [0, 1],
              [479,1],
              [768,2],
              [979,3],
              [1199,4]
            ],
            navigation : true,
            scrollPerPage: true
        });
    }
    ResizeIframe();
    
    $(window).load(function () {
        
    });
    $(window).resize(function () {
        //ResizeArtistWrapper('#block-views-artists-block .owl-wrapper-outer');
        ResizeIframe();
        if($('#node-291').length !== 0) {
            var ratio = 1.641;
            var windowHeight = $(window).height();
            var windowWidth = $(window).width();
            var maxWrapperWidth = $('#node-291').width() - 20;
            var maxWrapperHeight = maxWrapperWidth / ratio;
            if (windowHeight/maxWrapperHeight < 0.8) {
                maxWrapperHeight = windowHeight * 0.8;
                maxWrapperWidth = maxWrapperHeight * ratio;
            }
            $('#node-291 iframe.youtube').each(function() {
                $(this).width(maxWrapperWidth);
                $(this).height(maxWrapperHeight);
            });
        }
    });
    $(window).scroll(function() {
        
    });
});
;